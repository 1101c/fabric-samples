const Fabric_Client = require('fabric-client');
const path = require('path');
const util = require('util');
const os = require('os');
const express = require('express');
const router = express.Router();
const fabric_client = new Fabric_Client();
const channel = fabric_client.newChannel('mychannel');
const peer = fabric_client.newPeer('grpc://localhost:7051');
const order = fabric_client.newOrderer('grpc://localhost:7050');

channel.addPeer(peer);
channel.addOrderer(order);

var member_user = null;
const store_path = path.join(__dirname, '../hfc-key-store');
console.log('Store path:' + store_path);
var tx_id = null;

/**
 * Create new student
 */
router.post('/student', (req, res) => {

    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        return fabric_client.getUserContext('user1', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded user1 from persistence');
            member_user = user_from_store;
        } else {
            res.json({
                success: false,
                message: 'Failed to get user1.... run registerUser.js'
            });
            throw new Error('Failed to get user1.... run registerUser.js');
        }

        tx_id = fabric_client.newTransactionID();
        console.log("Assigning transaction_id: ", tx_id._transaction_id);

        var request = {
            chaincodeId: 'fabric-benchmark',
            fcn: 'createStudent',
            args: [req.body.studentNumber, req.body.name, req.body.program, req.body.gpa],
            chainId: 'mychannel',
            txId: tx_id
        };

        return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;
        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
            isProposalGood = true;
            console.log('Transaction proposal was good');
        } else {
            console.error('Transaction proposal was bad');
            res.json({
                success: false,
                message: 'Transaction proposal was bad'
            });
        }
        if (isProposalGood) {
            console.log(util.format(
                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            var transaction_id_string = tx_id.getTransactionID();
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise);
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://localhost:7053');

            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({ event_status: 'TIMEOUT' });
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {

                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    var return_status = { event_status: code, tx_id: transaction_id_string };
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status);
                    } else {
                        console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    reject(new Error('There was a problem with the eventhub ::' + err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            res.json({
                success: false,
                message: 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
            });
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        } else {
            console.error('Failed to order the transaction. Error code: ' + response.status);
        }

        if (results && results[1] && results[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
            res.json({
                success: true,
                message: "Successfully committed the change to the ledger by the peer"
            });
        } else {
            console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
            res.json({
                success: false,
                message: "Transaction failed to be committed to the ledger due to ::" + results[1].event_status
            });
        }
    }).catch((err) => {
        console.error('Failed to invoke successfully :: ' + err);
        res.json({
            success: false,
            message: 'Failed to invoke successfully :: ' + err
        });
    });

});

/**
 * Create new students
 */
router.post('/', (req, res) => {

    let students = [];

    req.body.students.forEach(student => {
        students.push(JSON.stringify(student));
    });

    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        return fabric_client.getUserContext('user1', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded user1 from persistence');
            member_user = user_from_store;
        } else {
            res.json({
                success: false,
                message: 'Failed to get user1.... run registerUser.js'
            });
            throw new Error('Failed to get user1.... run registerUser.js');
        }

        tx_id = fabric_client.newTransactionID();
        console.log("Assigning transaction_id: ", tx_id._transaction_id);

        var request = {
            chaincodeId: 'fabric-benchmark',
            fcn: 'createStudents',
            args: students,
            chainId: 'mychannel',
            txId: tx_id
        };

        return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;
        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
            isProposalGood = true;
            console.log('Transaction proposal was good');
        } else {
            console.error('Transaction proposal was bad');
            res.json({
                success: false,
                message: 'Transaction proposal was bad'
            });
        }
        if (isProposalGood) {
            console.log(util.format(
                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            var transaction_id_string = tx_id.getTransactionID();
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise);
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://localhost:7053');

            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({ event_status: 'TIMEOUT' });
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {

                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    var return_status = { event_status: code, tx_id: transaction_id_string };
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status);
                    } else {
                        console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    reject(new Error('There was a problem with the eventhub ::' + err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            res.json({
                success: false,
                message: 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
            });
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        } else {
            console.error('Failed to order the transaction. Error code: ' + response.status);
        }

        if (results && results[1] && results[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
            res.json({
                success: true,
                message: "Successfully committed the change to the ledger by the peer"
            });
        } else {
            console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
            res.json({
                success: false,
                message: "Transaction failed to be committed to the ledger due to ::" + results[1].event_status
            });
        }
    }).catch((err) => {
        console.error('Failed to invoke successfully :: ' + err);
        res.json({
            success: false,
            message: 'Failed to invoke successfully :: ' + err
        });
    });

});

/**
 * Get student by Student Number
 */
router.get('/:studentNumber', (req, res) => {
    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {

        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();

        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        return fabric_client.getUserContext('user1', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded user1 from persistence');
            member_user = user_from_store;
        } else {
            res.json({
                success: false,
                message: 'Failed to get user1.... run registerUser.js'
            });
            throw new Error('Failed to get user1.... run registerUser.js');
        }

        const request = {
            chaincodeId: 'fabric-benchmark',
            fcn: 'getStudent',
            args: [req.params.studentNumber]
        };
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                res.json({
                    success: false,
                    message: "error from query = " + query_responses[0]
                });
                console.error("error from query = ", query_responses[0]);
            } else {
                console.log("Response is ", query_responses[0].toString());
                res.json({
                    success: true,
                    student: JSON.parse(query_responses[0].toString())
                });
            }
        } else {
            res.json({
                success: true,
                message: 'Could not find any student with that Student Number'
            });
            console.log("No payloads were returned from query");
        }
    }).catch((err) => {
        res.json({
            success: false,
            message: 'Failed to query successfully :: ' + err
        });
        console.error('Failed to query successfully :: ' + err);
    });

});

/**
 * Get all Students
 */
router.get('/', (req, res) => {
    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {

        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();

        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        return fabric_client.getUserContext('user1', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded user1 from persistence');
            member_user = user_from_store;
        } else {
            res.json({
                success: false,
                message: 'Failed to get user1.... run registerUser.js'
            });
            throw new Error('Failed to get user1.... run registerUser.js');
        }

        const request = {
            chaincodeId: 'fabric-benchmark',
            fcn: 'getAllStudents',
            args: []
        };
        return channel.queryByChaincode(request);
    }).then((query_responses) => {
        console.log("Query has completed, checking results");
        // query_responses could have more than one  results if there multiple peers were used as targets
        if (query_responses && query_responses.length == 1) {
            if (query_responses[0] instanceof Error) {
                res.json({
                    success: false,
                    message: "error from query = " + query_responses[0]
                });
                console.error("error from query = ", query_responses[0]);
            } else {
                console.log("Response is ", query_responses[0].toString());
                res.json({
                    success: true,
                    students: JSON.parse(query_responses[0].toString())
                });
            }
        } else {
            res.json({
                success: true,
                message: 'Could not find any student with that Student Number'
            });
            console.log("No payloads were returned from query");
        }
    }).catch((err) => {
        res.json({
            success: false,
            message: 'Failed to query successfully :: ' + err
        });
        console.error('Failed to query successfully :: ' + err);
    });

});

/**
 * Delete student by Student Number
 */
router.get('/delete/:studentNumber', (req, res) => {
    Fabric_Client.newDefaultKeyValueStore({
        path: store_path
    }).then((state_store) => {
        fabric_client.setStateStore(state_store);
        var crypto_suite = Fabric_Client.newCryptoSuite();
        var crypto_store = Fabric_Client.newCryptoKeyStore({ path: store_path });
        crypto_suite.setCryptoKeyStore(crypto_store);
        fabric_client.setCryptoSuite(crypto_suite);

        return fabric_client.getUserContext('user1', true);
    }).then((user_from_store) => {
        if (user_from_store && user_from_store.isEnrolled()) {
            console.log('Successfully loaded user1 from persistence');
            member_user = user_from_store;
        } else {
            res.json({
                success: false,
                message: 'Failed to get user1.... run registerUser.js'
            });
            throw new Error('Failed to get user1.... run registerUser.js');
        }

        tx_id = fabric_client.newTransactionID();
        console.log("Assigning transaction_id: ", tx_id._transaction_id);

        var request = {
            chaincodeId: 'fabric-benchmark',
            fcn: 'deleteStudent',
            args: [req.params.studentNumber.toString()],
            chainId: 'mychannel',
            txId: tx_id
        };

        return channel.sendTransactionProposal(request);
    }).then((results) => {
        var proposalResponses = results[0];
        var proposal = results[1];
        let isProposalGood = false;
        if (proposalResponses && proposalResponses[0].response &&
            proposalResponses[0].response.status === 200) {
            isProposalGood = true;
            console.log('Transaction proposal was good');
        } else {
            console.error('Transaction proposal was bad');
            res.json({
                success: false,
                message: 'Transaction proposal was bad'
            });
        }
        if (isProposalGood) {
            console.log(util.format(
                'Successfully sent Proposal and received ProposalResponse: Status - %s, message - "%s"',
                proposalResponses[0].response.status, proposalResponses[0].response.message));

            var request = {
                proposalResponses: proposalResponses,
                proposal: proposal
            };

            var transaction_id_string = tx_id.getTransactionID();
            var promises = [];

            var sendPromise = channel.sendTransaction(request);
            promises.push(sendPromise);
            let event_hub = fabric_client.newEventHub();
            event_hub.setPeerAddr('grpc://localhost:7053');

            let txPromise = new Promise((resolve, reject) => {
                let handle = setTimeout(() => {
                    event_hub.disconnect();
                    resolve({ event_status: 'TIMEOUT' });
                }, 3000);
                event_hub.connect();
                event_hub.registerTxEvent(transaction_id_string, (tx, code) => {

                    clearTimeout(handle);
                    event_hub.unregisterTxEvent(transaction_id_string);
                    event_hub.disconnect();

                    var return_status = { event_status: code, tx_id: transaction_id_string };
                    if (code !== 'VALID') {
                        console.error('The transaction was invalid, code = ' + code);
                        resolve(return_status);
                    } else {
                        console.log('The transaction has been committed on peer ' + event_hub._ep._endpoint.addr);
                        resolve(return_status);
                    }
                }, (err) => {
                    reject(new Error('There was a problem with the eventhub ::' + err));
                });
            });
            promises.push(txPromise);

            return Promise.all(promises);
        } else {
            console.error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
            res.json({
                success: false,
                message: 'Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...'
            });
            throw new Error('Failed to send Proposal or receive valid response. Response null or status is not 200. exiting...');
        }
    }).then((results) => {
        console.log('Send transaction promise and event listener promise have completed');
        if (results && results[0] && results[0].status === 'SUCCESS') {
            console.log('Successfully sent transaction to the orderer.');
        } else {
            console.error('Failed to order the transaction. Error code: ' + response.status);
        }

        if (results && results[1] && results[1].event_status === 'VALID') {
            console.log('Successfully committed the change to the ledger by the peer');
            res.json({
                success: true,
                message: "Successfully deleted student"
            });
        } else {
            console.log('Transaction failed to be committed to the ledger due to ::' + results[1].event_status);
            res.json({
                success: false,
                message: "Transaction failed to be committed to the ledger due to ::" + results[1].event_status
            });
        }
    }).catch((err) => {
        console.error('Failed to invoke successfully :: ' + err);
        res.json({
            success: false,
            message: 'Failed to invoke successfully :: ' + err
        });
    });

});

module.exports = router;

