/*
# Copyright IBM Corp. All Rights Reserved.
#
# SPDX-License-Identifier: Apache-2.0
*/

'use strict';
const shim = require('fabric-shim');
const util = require('util');

let Chaincode = class {

  // The Init method is called when the Smart Contract 'fabstudent' is instantiated by the blockchain network
  // Best practice is to have any Ledger initialization in separate function -- see initLedger()
  async Init(stub) {
    console.info('=========== Instantiated fabstudent chaincode ===========');
    return shim.success();
  }

  // The Invoke method is called as a result of an application request to run the Smart Contract
  // 'fabstudent'. The calling application program has also specified the particular smart contract
  // function to be called, with arguments
  async Invoke(stub) {
    let ret = stub.getFunctionAndParameters();
    console.info(ret);

    let method = this[ret.fcn];
    if (!method) {
      console.error('no function of name:' + ret.fcn + ' found');
      throw new Error('Received unknown function ' + ret.fcn + ' invocation');
    }
    try {
      let payload = await method(stub, ret.params);
      return shim.success(payload);
    } catch (err) {
      console.log(err);
      return shim.error(err);
    }
  }

  async getStudent(stub, args) {
    if (args.length != 1) {
      throw new Error('Incorrect number of arguments. Expecting studentNumber ex: 100');
    }
    let studentNumber = args[0];

    let studentAsBytes = await stub.getState(studentNumber); //get the student from chaincode state
    if (!studentAsBytes || studentAsBytes.toString().length <= 0) {
      throw new Error(studentNumber + ' does not exist: ');
    }
    console.log(studentAsBytes.toString());
    return studentAsBytes;
  }

  async createStudent(stub, args) {
    console.info('============= START : Create Student ===========');
    if (args.length != 4) {
      throw new Error('Incorrect number of arguments. Expecting 4');
    }

    var student = {
      studentNumber: args[0],
      name: args[1],
      program: args[2],
      gpa: args[3]
    };

    await stub.putState(args[0], Buffer.from(JSON.stringify(student)));
    console.info('============= END : Create Student ===========');
  }

  async createStudents(stub, args) {
    console.info('============= START : Create Students ===========');

    let students = args;

    for (let i = 0; i < students.length; i++) {
      await stub.putState('STU' + i, Buffer.from(students[i]));
      console.info('Added <--> ', students[i]);
    }
  }

  async getAllStudents(stub, args) {

    let startKey = '';
    let endKey = '';

    let iterator = await stub.getStateByRange(startKey, endKey);

    let allResults = [];
    while (true) {
      let res = await iterator.next();

      if (res.value && res.value.value.toString()) {
        let jsonRes = {};
        console.log(res.value.value.toString('utf8'));

        jsonRes.Key = res.value.key;
        try {
          jsonRes.Record = JSON.parse(res.value.value.toString('utf8'));
        } catch (err) {
          console.log(err);
          jsonRes.Record = res.value.value.toString('utf8');
        }
        allResults.push(jsonRes);
      }
      if (res.done) {
        console.log('end of data');
        await iterator.close();
        console.info(allResults);
        return Buffer.from(JSON.stringify(allResults));
      }
    }
  }

  async changeStudentGpa(stub, args) {
    console.info('============= START : changeStudentGpa ===========');
    if (args.length != 2) {
      throw new Error('Incorrect number of arguments. Expecting 2');
    }

    let studentAsBytes = await stub.getState(args[0]);
    let student = JSON.parse(studentAsBytes);
    student.gpa = args[1];

    await stub.putState(args[0], Buffer.from(JSON.stringify(student)));
    console.info('============= END : changeStudentOwner ===========');
  }

    // Delete a student
    async deleteStudent(stub, args) {
      if (args.length != 1) {
        throw new Error('Incorrect number of arguments. Expecting 1');
      }
  
      let A = args[0];
  
      // Delete the key from the state in ledger
      await stub.deleteState(A);
    }
};

shim.start(new Chaincode());
